package com.message.ui.msg.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.msg.pojo.MsgInfo;
import com.message.admin.sys.service.SysInfoService;
import com.message.ui.comm.controller.BaseController;
import com.message.ui.msg.service.UiMsgInfoService;
import com.system.handle.model.ResponseFrame;

/**
 * msgInfo的Controller
 * @author autoCode
 * @date 2017-12-27 15:24:04
 * @version V1.0.0
 */
@Controller
public class UiMsgInfoController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UiMsgInfoController.class);

	@Autowired
	private UiMsgInfoService uiMsgInfoService;
	@Autowired
	private SysInfoService sysInfoService;
	
	@RequestMapping(value = "/msgInfo/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/msg/msgInfo-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/msgInfo/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response, MsgInfo msgInfo) {
		ResponseFrame frame = null;
		try {
			frame = uiMsgInfoService.pageQuery(msgInfo);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
		}
		writerJson(response, frame);
	}

}