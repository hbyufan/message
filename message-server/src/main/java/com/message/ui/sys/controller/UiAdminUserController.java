package com.message.ui.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.ui.comm.controller.BaseController;
import com.message.ui.sys.pojo.AdminUser;
import com.message.ui.sys.service.AdminUserService;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * admin_user的Controller
 * @author autoCode
 * @date 2017-12-27 15:24:04
 * @version V1.0.0
 */
@Controller
public class UiAdminUserController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UiAdminUserController.class);

	@Autowired
	private AdminUserService adminUserService;
	
	/**
	 * ajax登录
	 * @return
	 */
	@RequestMapping(value = "/adminUser/json/login")
	@ResponseBody
	public void login(HttpServletRequest request, HttpServletResponse response, AdminUser adminUser) {
		ResponseFrame frame = null;
		try {
			frame = adminUserService.login(adminUser);
			if(ResponseCode.SUCC.getCode() == frame.getCode().intValue()) {
				AdminUser user = (AdminUser) frame.getBody();
				setSessionAdminUser(request, user);
			}
		} catch (Exception e) {
			LOGGER.error("登录异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	/**
	 * 退出
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/adminUser/f-view/logout")
	public String exit(HttpServletRequest request) {
		removeSessionAdminUser(request);
		return "redirect:/index.jsp";
	}
	
	/**
	 * 跳转到管理页
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/adminUser/f-view/main")
	public String main(HttpServletRequest request) {
		return "admin/main";
	}
	
	@RequestMapping(value = "/adminUser/f-view/manager")
	public String manger(HttpServletRequest request) {
		return "admin/sys/user-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/adminUser/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response, AdminUser adminUser) {
		ResponseFrame frame = null;
		try {
			frame = adminUserService.pageQuery(adminUser);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
		}
		writerJson(response, frame);
	}

	/**
	 * 跳转到编辑页[包含新增和编辑]
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/adminUser/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, String id) {
		if(FrameStringUtil.isNotEmpty(id)) {
			modelMap.put("adminUser", adminUserService.get(id));
		}
		return "admin/sys/user-edit";
	}

	/**
	 * 保存
	 * @return
	 */
	@RequestMapping(value = "/adminUser/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response, AdminUser adminUser) {
		ResponseFrame frame = null;
		try {
			frame = adminUserService.saveOrUpdate(adminUser);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "/adminUser/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		ResponseFrame frame = null;
		try {
			frame = adminUserService.delete(id);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
}