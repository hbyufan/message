package com.message.admin.send.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.message.admin.send.pojo.SendSms;

/**
 * send_sms的Dao
 * @author autoCode
 * @date 2017-12-13 11:15:58
 * @version V1.0.0
 */
public interface SendSmsDao {

	public abstract void save(SendSms sendSms);

	public abstract void update(SendSms sendSms);

	public abstract void delete(@Param("id")String id);

	public abstract SendSms get(@Param("id")String id);

	public abstract List<SendSms> findSendSms(SendSms sendSms);
	
	public abstract int findSendSmsCount(SendSms sendSms);

	public abstract void updateWaitToIng(@Param("servNo")String servNo, @Param("num")Integer num);

	public abstract List<SendSms> findIng(@Param("servNo")String servNo);

	public abstract void updateStatus(@Param("id")String id, @Param("status")Integer status);
}