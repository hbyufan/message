<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>定时任务-编辑系统用户</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-xs">
		<input type="hidden" id="id" value="${userInfo.id}">
  		<div class="form-group">
			<label for="sysNo" class="col-sm-4">系统编码 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:select id="sysNo" items="${sysInfos}" headerKey="" headerValue="-- 请选择系统 --" cssCls="form-control input-sm" value="${userInfo.sysNo}"/></div>
		</div>
  		<div class="form-group">
			<label for="userId" class="col-sm-4">用户编号 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="userId" placeholder="用户编码" value="${userInfo.userId}"></div>
		</div>
  		<div class="form-group">
			<label for="phone" class="col-sm-4">手机号</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="phone" placeholder="手机号[多个;分隔]" value="${userInfo.phone}"></div>
		</div>
  		<div class="form-group">
			<label for="email" class="col-sm-4">邮箱</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="email" placeholder="邮箱[多个;分隔]" value="${userInfo.email}"></div>
		</div>
		<hr/>
  		<div class="form-group text-right">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var _saveMsg = $('#saveMsg').empty();
			
			var _id = $('#id').val();
			var _sysNo = $('#sysNo');
			if(JUtil.isEmpty(_sysNo.val())) {
				_saveMsg.append('请选择系统');
				_sysNo.focus();
				return;
			}
			var _userId = $('#userId');
			if(JUtil.isEmpty(_userId.val())) {
				_saveMsg.append('请输入用户编码');
				_userId.focus();
				return;
			}

			var _saveBtn = $('#saveBtn');
			var _orgVal = _saveBtn.html();
			_saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/userInfo/f-json/save',
				data : {
					id: _id,
					sysNo: _sysNo.val(),
					userId: _userId.val(),
					phone: $('#phone').val(),
					email: $('#email').val()
				},
				success : function(json) {
					if (json.code === 0) {
						_saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						_saveMsg.append(JUtil.msg.ajaxErr);
					else
						_saveMsg.append(json.message);
					_saveBtn.removeAttr('disabled').html(_orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>